function adaboost()

% flags
flag_data_subset = 0;
flag_extract_features = 0;
flag_parpool = 1;
flag_boosting = 0;

% parpool
if flag_parpool
    delete(gcp('nocreate'));
    parpool(4);
end

% unit tests
test_sum_rect();
test_filters();

% constants
if flag_data_subset
    N_pos = 5000;
    N_neg = 5000;
else
    %N_pos = 11838;
    %N_neg = 45356;
    
    N_pos = 11838;
    N_neg = 25356;
end
N = N_pos + N_neg;
w = 16;
h = 16;

% (1) haar filter

% load images
if flag_extract_features
    tic;
    I = zeros(N, h, w);
    for i=1:N_pos
        I(i,:,:) = rgb2gray(imread(sprintf('newface16/face16_%06d.bmp',i), 'bmp'));
    end
    for i=1:N_neg
        I(N_pos+i,:,:) = rgb2gray(imread(sprintf('nonface16/nonface16_%06d.bmp',i), 'bmp'));
    end
    fprintf('Loading images took %.2f secs.\n', toc);
end

% construct filters
A = filters_A();
B = filters_B();
C = filters_C();
D = filters_D();
if flag_data_subset
    filters = [A(1:1000,:); B(1:1000,:); C(1:1000,:); D(1:900,:)];
else
    filters = [A; B; C; D];
end

% extract features
if flag_extract_features
    tic;
    I = normalize(I);
    II = integral(I);
    features = compute_features(II, filters);
    %clear I;
    %clear II;
    save('features.mat', '-v7.3', 'features');
    fprintf('Extracting %d features from %d images took %.2f secs.\n', size(filters, 1), N, toc);
else
    load('features.mat','features');
end

% perform boosting
if(flag_boosting == 1)
    fprintf('Running AdaBoost with %d features from %d images.\n', size(filters, 1), N);
    tic;
    %% implement this
    D_t = repmat(1/N, 1, N);
    y = [ones(1, N_pos) ones(1,N_neg)*(-1)];
    num_wf = 150;
    alpha = zeros(num_wf, 1);
    h = zeros(num_wf, N);
    idx = zeros(num_wf, 1);
    theta = zeros(num_wf, 1);
    polarity = zeros(num_wf, 1);
    weak_error = zeros(num_wf, 1);
    s = ones(num_wf, 1);
    strong_error = zeros(num_wf, 1);
    for i = 1:num_wf
        [theta_i, weak_err, pol] = ComputeTheshold(features, y, D_t, N_pos);
        [weak_err_value, weak_err_idx] = min(weak_err);
        idx(i) = weak_err_idx;
        theta(i) = theta_i(weak_err_idx);
        weak_error(i) = weak_err_value;
        polarity(i) = pol(weak_err_idx);
        if pol(weak_err_idx) == 1
            h_t = sign(features(weak_err_idx,:)-repmat(theta(i), 1, N));
        else
            h_t = -1 * sign(features(weak_err_idx,:)-repmat(theta(i), 1, N));
        end
        alpha_t = 1/2 * log((1-weak_err_value)/weak_err_value);
        Z_t = sum(D_t .* exp(-1* y .* (alpha_t * h_t)));
        D_t = 1/Z_t * D_t .* exp(-1* y .* (alpha_t * h_t)); 
        alpha(i) = alpha_t;
        h(i, :) = h_t;
        F = alpha'*h;
        H = sign(F);
        strong_error(i) = sum(H~=y)/size(y,2);
        save(sprintf('/Users/ht/Desktop/project2_code_and_data/iteration/iteration_%03d.mat', i), 'weak_err', 'F');
    end
    save('adaboost.mat','-v7.3','alpha','idx','theta','s', 'polarity', 'weak_error', 'strong_error','y');
    fprintf('Running AdaBoost %d with features from %d images took %.2f secs.\n', size(filters, 1), N, toc);
else
    load('adaboost.mat','alpha','idx','theta','s', 'polarity', 'weak_error', 'strong_error', 'y');
    y = [ones(1, N_pos) ones(1,N_neg)*(-1)];%temp
end

% (1) top-20 haar filters
%% implement this
figure;
for i = 1:20
    subplot(4,5,i);
    plot_filter(filters(idx(i), :));
end

% (2) plot training error
%% implement this
T = 1:size(strong_error,1);
figure;
plot(T,strong_error);
xlabel('Number of steps T');
ylabel('Training error');
title('Training error of strong classifier VS Number of steps T');
% (3) training errors of top-1000 weak classifiers
%% implement this
iter0 = load('/Users/ht/Desktop/project2_code_and_data/iteration/iteration_001.mat');
iter10 = load('/Users/ht/Desktop/project2_code_and_data/iteration/iteration_010.mat');
iter50 = load('/Users/ht/Desktop/project2_code_and_data/iteration/iteration_050.mat');
iter100 = load('/Users/ht/Desktop/project2_code_and_data/iteration/iteration_100.mat');
error0 = sort(iter0.weak_err);
error10 = sort(iter10.weak_err);
error50 = sort(iter50.weak_err);
error100 = sort(iter100.weak_err);
figure;
plot(1:1000, error0(1:1000), '-', 1:1000, error10(1:1000), '--', 1:1000, error50(1:1000), '-.',1:1000, error100(1:1000), '-.');
legend('T = 0','T = 10', 'T = 50', 'T = 100');
title('Training error of weak classifiers at T=0,10,50,100');
% (4) negative positive histograms
%% implement this
F10 = iter10.F;
F50 = iter50.F;
F100 = iter100.F;
[F10_pos, F10_neg] = get_hist(F10, y);
[F50_pos, F50_neg] = get_hist(F50, y);
[F100_pos, F100_neg] = get_hist(F100, y);
plot_hist(F10_pos, F10_neg, 'Histogram(T = 10)');
plot_hist(F50_pos, F50_neg, 'Histogram(T = 50)');
plot_hist(F100_pos, F100_neg, 'Histogram(T = 100)');
% (5) plot ROC curves
%% implement this
[TP_F10, FP_F10] = get_ROC(iter10.F, y, N_pos, N_neg);
[TP_F50, FP_F50] = get_ROC(iter50.F, y, N_pos, N_neg);
[TP_F100, FP_F100] = get_ROC(iter100.F, y, N_pos, N_neg);
figure;
hold on;
plot_ROC(TP_F10, FP_F10, 'ROC(T = 10)');
plot_ROC(TP_F50, FP_F50, 'ROC(T = 50)');
plot_ROC(TP_F100, FP_F100, 'ROC(T = 100)');
legend('T = 10', 'T = 50', 'T = 100');
title('ROC at T=0,10,50,100');
hold off;

% (6) detect faces
%% implement this
load('adaboost.mat','alpha','idx','theta','s', 'polarity', 'weak_error', 'strong_error', 'y');
disp('ada loaded.');

orig_img = zeros(3,960,1280);%img size: 1280*960
for i = 1:3
    orig_img(i,:,:) = rgb2gray(imread(sprintf('/Users/ht/Desktop/Testing_images/Face_%d.jpg',i), 'jpg'));
end

scale = 0.15:0.05:1.1;
dim = [16 16];
img_temp = zeros(960,1280);

filters_selected = cell(size(alpha,1),2);
for k = 1:size(filters_selected,1)
    filters_selected{k,1} = filters{idx(k), 1};
    filters_selected{k,2} = filters{idx(k), 2};
end

for i = 1:size(orig_img, 1)
    img_temp(:,:) = orig_img(i,:,:);
    figure;
    imshow(uint8(img_temp));
    hold on;
    scores_pass = cell(1, size(scale,2));
    loc_pass = cell(1, size(scale,2));
    for n = 1:size(scale, 2)
        [segments, loc] = get_segments(img_temp, dim, scale(n));
        I_t = normalize_t(segments, size(segments,1));
        II_t = integral_t(I_t,size(I_t,1));
        features_t = compute_features(II_t, filters_selected);
        scores = prediction(features_t, theta, polarity, alpha);
        pos_idx = find(scores > 2.2);
        scores_pass{n} = scores(pos_idx);
        loc_pass{n} = loc(pos_idx, :);
    end
    loc_tp = loc_pass{1};
    scores_tp = scores_pass{1};
    for k = 2:size(scale, 2)
        loc_tp = [loc_tp;loc_pass{k}];
        scores_tp = [scores_tp; scores_pass{k}];
    end
    
    count = 0;
    for j = 1:size(loc_tp,1)
        count = count + 1;
        for l = 1+count:size(loc_tp,1)
            if abs(loc_tp(j,1)/loc_tp(j,3)-loc_tp(l,1)/loc_tp(l,3)) < 4/loc_tp(j,3)...
                    && abs(loc_tp(j,2)/loc_tp(j,3)-loc_tp(l,2)/loc_tp(l,3)) < 4/loc_tp(j,3)
                if scores_tp(j) < scores_tp(l)
                    scores_tp(j) = 0;
                else
                    scores_tp(l) = 0;
                end
            end
        end
    end
    
    loc_final = loc_tp(scores_tp ~= 0, :);
    for j=1:size(loc_final, 1)
        rectangle('position', [loc_final(j,2), loc_final(j,1), 16, 16]/loc_final(j,3), 'EdgeColor', 'g')    
    end
    hold off;      
end
disp('Done.');

end

%% filters

function features = compute_features(II, filters)
features = zeros(size(filters, 1), size(II, 1));
for j = 1:size(filters, 1)
    [rects1, rects2] = filters{j,:};
    features(j,:) = apply_filter(II, rects1, rects2);
end
end

function I = normalize(I)
[N,~,~] = size(I);
for i = 1:N
    image = I(i,:,:);
    sigma = std(image(:));
    I(i,:,:) = I(i,:,:) / sigma;
end
end

function II = integral(I)
[N,H,W] = size(I);
II = zeros(N,H+1,W+1);
for i = 1:N
    image = squeeze(I(i,:,:));
    II(i,2:H+1,2:W+1) = cumsum(cumsum(double(image), 1), 2);
end
end

function sum = apply_filter(II, rects1, rects2)
sum = 0;
% white rects
for k = 1:size(rects1,1)
    r1 = rects1(k,:);
    w = r1(3);
    h = r1(4);
    sum = sum + sum_rect(II, [0, 0], r1) / (w * h * 255);
end
% black rects
for k = 1:size(rects2,1)
    r2 = rects2(k,:);
    w = r2(3);
    h = r2(4);
    sum = sum - sum_rect(II, [0, 0], r2) / (w * h * 255);
end
end

function result = sum_rect(II, offset, rect)
x_off = offset(1);
y_off = offset(2);

x = rect(1);
y = rect(2);
w = rect(3);
h = rect(4);

a1 = II(:, y_off + y + h, x_off + x + w);
a2 = II(:, y_off + y + h, x_off + x);
a3 = II(:, y_off + y,     x_off + x + w);
a4 = II(:, y_off + y,     x_off + x);

result = a1 - a2 - a3 + a4;
end

function rects = filters_A()
count = 1;
w_min = 4;
h_min = 4;
w_max = 16;
h_max = 16;
rects = cell(1,2);
for w = w_min:2:w_max
    for h = h_min:h_max
        for x = 1:(w_max-w)
            for y = 1:(h_max-h)
                r1_x = x;
                r1_y = y;
                r1_w = w/2;
                r1_h = h;
                r1 = [r1_x, r1_y, r1_w, r1_h];
                
                r2_x = r1_x + r1_w;
                r2_y = r1_y;
                r2_w = w/2;
                r2_h = h;
                r2 = [r2_x, r2_y, r2_w, r2_h];
                
                rects{count, 1} = r1; % white
                rects{count, 2} = r2; % black
                count = count + 1;
            end
        end
    end
end
end

function rects = filters_B()
count = 1;
w_min = 4;
h_min = 4;
w_max = 16;
h_max = 16;
rects = cell(1,2);
for w = w_min:w_max
    for h = h_min:2:h_max
        for x = 1:(w_max-w)
            for y = 1:(h_max-h)
                r1_x = x;
                r1_y = y;
                r1_w = w;
                r1_h = h/2;
                r1 = [r1_x, r1_y, r1_w, r1_h];
                
                r2_x = r1_x;
                r2_y = r1_y + r1_h;
                r2_w = w;
                r2_h = h/2;
                r2 = [r2_x, r2_y, r2_w, r2_h];
                
                rects{count, 1} = r2; % white
                rects{count, 2} = r1; % black
                count = count + 1;
            end
        end
    end
end
end

function rects = filters_C()
count = 1;
w_min = 6;
h_min = 4;
w_max = 16;
h_max = 16;
rects = cell(1,2);
for w = w_min:3:w_max
    for h = h_min:h_max
        for x = 1:(w_max-w)
            for y = 1:(h_max-h)
                r1_x = x;
                r1_y = y;
                r1_w = w/3;
                r1_h = h;
                r1 = [r1_x, r1_y, r1_w, r1_h];
                
                r2_x = r1_x + r1_w;
                r2_y = r1_y;
                r2_w = w/3;
                r2_h = h;
                r2 = [r2_x, r2_y, r2_w, r2_h];
                
                r3_x = r1_x + r1_w + r2_w;
                r3_y = r1_y;
                r3_w = w/3;
                r3_h = h;
                r3 = [r3_x, r3_y, r3_w, r3_h];
                
                rects{count, 1} = [r1; r3]; % white
                rects{count, 2} = r2; % black
                count = count + 1;
            end
        end
    end
end
end

function rects = filters_D()
count = 1;
w_min = 6;
h_min = 6;
w_max = 16;
h_max = 16;
rects = cell(1,2);
for w = w_min:2:w_max
    for h = h_min:2:h_max
        for x = 1:(w_max-w)
            for y = 1:(h_max-h)
                r1_x = x;
                r1_y = y;
                r1_w = w/2;
                r1_h = h/2;
                r1 = [r1_x, r1_y, r1_w, r1_h];
                
                r2_x = r1_x+r1_w;
                r2_y = r1_y;
                r2_w = w/2;
                r2_h = h/2;
                r2 = [r2_x, r2_y, r2_w, r2_h];
                
                r3_x = x;
                r3_y = r1_y+r1_h;
                r3_w = w/2;
                r3_h = h/2;
                r3 = [r3_x, r3_y, r3_w, r3_h];
                
                r4_x = r1_x+r1_w;
                r4_y = r1_y+r2_h;
                r4_w = w/2;
                r4_h = h/2;
                r4 = [r4_x, r4_y, r4_w, r4_h];
                
                rects{count, 1} = [r2; r3]; % white
                rects{count, 2} = [r1; r4]; % black
                count = count + 1;
            end
        end
    end
end
end

function test_sum_rect()
% 1
I = zeros(1,16,16);
I(1,2:4,2:4) = 1;
%disp(squeeze(I(1,:,:)));
II = integral(I);
assert(sum_rect(II, [0, 0], [2, 2, 3, 3]) == 9);
assert(sum_rect(II, [0, 0], [10, 10, 2, 2]) == 0);

% 2
I = zeros(1,16,16);
I(1,10:16,10:16) = 1;
%disp(squeeze(I(1,:,:)));
II = integral(I);
assert(sum_rect(II, [0, 0], [10, 10, 2, 2]) == 4);

% 3
I = zeros(1,16,16);
I(1,:,:) = 0;
I(1,3:6,3:6) = 1;
I(1,3:6,11:14) = 1;
%disp(squeeze(I(1,:,:)));
II = integral(I);
assert(sum_rect(II, [0, 0], [11, 3, 6, 6]) == 16);

% 4
I = zeros(1,16,16);
I(1,:,:) = 0;
I(1,3:6,3:6) = 1;
I(1,3:6,11:14) = 1;
%disp(squeeze(I(1,:,:)));
II = integral(I);
assert(sum_rect(II, [0, 0], [3, 4, 4, 4]) == 12);
assert(sum_rect(II, [0, 0], [7, 4, 4, 4]) == 0);
assert(sum_rect(II, [0, 0], [11, 4, 4, 4]) == 12);
assert(sum_rect(II, [0, 0], [3, 3, 4, 4]) == 16);
assert(sum_rect(II, [0, 0], [11, 3, 4, 4]) == 16);

end

function test_filters()

% A
I = zeros(1,16,16);
I(1,:,:) = 255;
I(1,5:8,5:8) = 0;
II = integral(I);
%disp(squeeze(I(1,:,:)));
rects = filters_A();
max_size = 0;
max_sum = 0;
for i = 1:size(rects, 1)
    [r1s, r2s] = rects{i,:};
    f_sum = apply_filter(II, r1s, r2s);
    f_size = r1s(1,3) * r1s(1,4) + r2s(1,3) * r2s(1,4);
    if(and(f_sum > max_sum, f_size == 4*4*2))
        max_size = f_size;
        max_sum = f_sum;
        min_f = [r1s, r2s];
    end
end
assert(max_sum == 1);
assert(max_size == 4*4*2);
assert(isequal(min_f, [1 5 4 4 5 5 4 4]));

% B
I = zeros(1,16,16);
I(1,:,:) = 255;
I(1,2:5,2:5) = 0;
II = integral(I);
%disp(squeeze(I(1,:,:)));
rects = filters_B();
max_size = 0;
max_sum = 0;
for i = 1:size(rects, 1)
    [r1s, r2s] = rects{i,:};
    f_sum = apply_filter(II, r1s, r2s);
    f_size = r1s(1,3) * r1s(1,4) + r2s(1,3) * r2s(1,4);
    if(and(f_sum > max_sum, f_size == 4*4*2))
        max_size = f_size;
        max_sum = f_sum;
        min_f = [r1s, r2s];
    end
end
assert(max_sum == 1);
assert(max_size == 4*4*2);
assert(isequal(min_f, [2 6 4 4 2 2 4 4]));

% C
I = zeros(1,16,16);
I(1,:,:) = 0;
I(1,3:6,3:6) = 255;
I(1,3:6,11:14) = 255;
II = integral(I);
%disp(squeeze(I(1,:,:)));
rects = filters_C();
max_size = 0;
max_sum = 0;
for i = 1:size(rects, 1)
    [r1s, r2s] = rects{i,:};
    f_sum = apply_filter(II, r1s, r2s);
    f_size = r1s(1,3) * r1s(1,4) + r1s(2,3) * r1s(2,4) + r2s(1,3) * r2s(1,4);
    if(and(f_sum > max_sum, f_size == 4*4*3))
        max_size = f_size;
        max_sum = f_sum;
        min_f = [reshape(r1s', [1,8]), r2s];
    end
end
assert(max_sum == 2);
assert(max_size == 4*4*3);
assert(isequal(min_f, [3 3 4 4 11 3 4 4 7 3 4 4]));

% D
I = zeros(1,16,16);
I(1,:,:) = 255;
I(1,2:5,2:5) = 0;
I(1,6:9,6:9) = 0;
II = integral(I);
%disp(squeeze(I(1,:,:)));
rects = filters_D();
max_size = 0;
max_sum = 0;
for i = 1:size(rects, 1)
    [r1s, r2s] = rects{i,:};
    f_sum = apply_filter(II, r1s, r2s);
    f_size = r1s(1,3) * r1s(1,4) + r1s(2,3) * r1s(2,4) + r2s(1,3) * r2s(1,4) + r2s(2,3) * r2s(2,4);
    if(and(f_sum > max_sum, f_size == 4*4*4))
        max_size = f_size;
        max_sum = f_sum;
        min_f = [reshape(r1s', [1,8]), reshape(r2s', [1,8])];
    end
end
assert(max_sum == 2);
assert(max_size == 4*4*4);
assert(isequal(min_f, [6 2 4 4 2 6 4 4 2 2 4 4 6 6 4 4]));

end
