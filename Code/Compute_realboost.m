function [ Z, ht ] = Compute_realboost( features, nbins, y, D_t )
num_filter = size(features,1);
num_image = size(features,2);
%D_t = 1/num_image * zeros(1,num_image);
p_t = zeros(1,nbins);
q_t = zeros(1,nbins);
Z = zeros(num_filter, 1);
ht = zeros(num_filter, num_image);
for i = 1:num_filter
    max_tp = max(features(i,:));
    min_tp = min(features(i,:));
    partition = linspace(min_tp,max_tp,nbins+1);
    for j = 1:nbins
        temp = (features(i,:) <= partition(j+1)) - (features(i,:) < partition(j));
        temp = temp .* y;
        p_t(j) = sum(D_t(temp > 0));
        q_t(j) = sum(D_t(temp < 0));
        if p_t(j) == 0
            p_t(j) = 10^(-2);
        end
        if q_t(j) == 0
            q_t(j) = 10^(-2);
        end
        ht(i,:) = ht(i,:) + temp .* y .* (1/2*log(p_t(j)/q_t(j)));
    end
    Z(i) = sum(sqrt(p_t.*q_t))*2;
end
 
