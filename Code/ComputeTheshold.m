function [ theta, weak_error, pol ] = ComputeTheshold...
( features, y, D_t, N_pos )
%theta is the threshold for each filter
%features are computed by integral images and filters
n_filter = size(features, 1);
n_image = size(features, 2);
n_space = 100; %for theta
theta = zeros(n_filter, 1);
weak_error = zeros(n_filter, 1);
theta_temp = zeros(n_filter, n_space);
err = zeros(n_space, 1);
dir = zeros(n_space, 1);
pol = zeros(n_filter, 1);
h = zeros(n_filter, n_image);

%Find each threshold which can minimize error for each weak filter
min_pos = min(features(:, 1:N_pos),[], 2);
min_neg = min(features(:, (N_pos+1):end),[], 2);
max_pos = max(features(:, 1:N_pos),[], 2);
max_neg = max(features(:, (N_pos+1):end),[], 2);
for i = 1:n_filter
    if abs(min_pos(i) - max_neg(i)) < abs(max_pos(i) - min_neg(i))
        if min_pos(i) < max_neg(i)
            p1 = min_pos(i);
            p2 = max_neg(i);
        else
            p2 = min_pos(i);
            p1 = max_neg(i);
        end
    else
        if max_pos(i) < min_neg(i)
           p1 = max_pos(i);
           p2 = min_neg(i);
        else
            p2 = max_pos(i);
            p1 = min_neg(i);
        end
    end
theta_temp(i,:) = linspace(p1, p2, n_space);
end

for i = 1:n_filter
    for j =  1:n_space
        temp1 = sign(features(i,:)-repmat(theta_temp(i,j), 1, n_image));
        err1 = sum(D_t.*(temp1~=y));
        err2 = 1 - err1;
        if  err1 < err2
            dir(j) = 1;
            err(j) = err1;
        else
            dir(j) = -1;
            err(j) = err2;
        end
    end
    [errval, index_err] = min(err);
    theta(i) = theta_temp(i,index_err);
    pol(i) = dir(index_err);
    weak_error(i) = errval;
end
end

